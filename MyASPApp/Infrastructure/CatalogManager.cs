﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Runtime.Remoting;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MyASPApp.Models;

namespace MyASPApp.Infrastructure
{
    public class CookieEventArgs : EventArgs
    {
        public HttpCookieCollection Cookies { get; set; }
        public Int32 ProdId { get; set; }
        public string Text { get; set; }

        public CookieEventArgs(HttpCookieCollection cookieCollection, Int32 prodId, string text)
        {
            Cookies = cookieCollection;
            ProdId = prodId;
            Text = text;
        }
    }

    public class CatalogManager
    {
        private ApplicationDbContext _Db;
        private event EventHandler<CookieEventArgs> _saveToCookie;

        public CatalogManager()
        {
            _Db = new ApplicationDbContext();
            _saveToCookie += (s, e) =>
            {
                HttpCookie cookie = e.Cookies["SaveText"] ?? new HttpCookie("LastProds");
                cookie.Values[e.ProdId.ToString()] = e.Text;
                cookie.Expires = DateTime.Now.AddDays(2);
            };
        }

        //1
        public ICollection<ApplicationUser> GetAllUsers()
        {
            return _Db.Users.ToList();
        }

        public ApplicationUser GetUserAsync(string userId)
        {
            return _Db.Users.SingleOrDefault(u => u.Id == userId);
        }
        //2
        public Feedback AddFeedback(string usrId, Int32 pdctId)
        {
            var user = _Db.Users.SingleOrDefault(u => u.Id == usrId);
            var product = _Db.Products.SingleOrDefault(p => p.Id == pdctId);
            
            if (user == null || product == null)
                throw new NullReferenceException("Invalid userId or prodId");

            var feedback = new Feedback( user, product );

            _Db.Feedbacks.Add( feedback );
            _Db.SaveChanges();
            return feedback;
        }

        public void AddFeedback(string usrId, Int32 pdctId, Feedback feedback)
        {
            var user = _Db.Users.SingleOrDefault(u => u.Id == usrId);
            var product = _Db.Products.SingleOrDefault(p => p.Id == pdctId);

            if (user == null || product == null)
                throw new NullReferenceException("Invalid userId or prodId");

            feedback.ApplicationUser = user;
            feedback.Product = product;

            _Db.Feedbacks.Add(feedback);
            _Db.SaveChanges();
        }

        public void DelFeedback(Int32 feedbackId)
        {
            _Db.Feedbacks.Remove( _Db.Feedbacks.SingleOrDefault(f => f.Id == feedbackId) );
            _Db.SaveChanges();
        }

        public Feedback GetFeedback(Int32 feedbackId)
        {
            return _Db.Feedbacks.Find(feedbackId);
        }

        public List<Feedback> GetFeedbacksByProduct(Int32 prodId)
        {
            return _Db.Feedbacks.Where(feedback => feedback.Product.Id == prodId).ToList();
        } 

        //3
        public ICollection<Category> GetAllCategoryes()
        {
            return _Db.Categories.ToList();
        }

        public Category GetCategoryesAsync(Int32 categoryId)
        {
            return _Db.Categories.Find(categoryId);
        }

        public void AddCategory(Category category)
        {
            _Db.Categories.Add(category);
            _Db.SaveChanges();
        }

        public void DelCategory(Int32 categoryId)
        {
            _Db.Categories.Remove( _Db.Categories.Find(categoryId) );
            _Db.SaveChanges();
        }

        //4
        public ICollection<SubCategory> GetSubcategotyesByCategory(Int32 categoryId)
        {
            var category = _Db.Categories.SingleOrDefault(c => c.Id == categoryId);
            return category.SubCategories;
        }

        public ICollection<SubCategory> GetAllSubcategotyes()
        {
            return _Db.SubCategories.ToList();
        }

        public void AddSubcategory(SubCategory subCategory, Int32 categoryId)
        {
            var category = _Db.Categories.SingleOrDefault(cat => cat.Id == categoryId);
            subCategory.Category = category;
            _Db.SubCategories.Add(subCategory);
            _Db.SaveChanges();
        }

        public void DelSubcategory(Int32 subcategoryId)
        {
            _Db.SubCategories.Remove(_Db.SubCategories.Find(subcategoryId));
            _Db.SaveChanges();
        }

        public SubCategory GetSubcategory(Int32 subcategoryId)
        {
            return _Db.SubCategories.Find(subcategoryId);
        } 

        //5
        public ICollection<Product> GetAllProducts()
        {
            return _Db.Products.ToList();
        }

        public ICollection<Product> GetProductsBySubcategory(Int32 subcategoryId)
        {
            return _Db.SubCategories.SingleOrDefault(subcat => subcat.Id == subcategoryId).Products;
        }

        public ICollection<Product> GetProductsByCategory(Int32 categoryId)
        {
            var category = _Db.Categories.SingleOrDefault(cat => cat.Id == categoryId);
            List<Product> products = new List<Product>();
            foreach (var subcategory in category.SubCategories)
            {
                products.AddRange(subcategory.Products);
            }
            /*Select products.*
            from subcategoryes, categoryes, prodyucts
            where subcategory.categoryId = category.Id and
            products.subcategoryId = subcategory.id;*/
            return products;
        }

        public void AddProduct(Product product, string userId, Int32 subcategoryId)
        {
            product.ApplicationUser = _Db.Users.SingleOrDefault(u => u.Id == userId);
            product.SubCategory = _Db.SubCategories.SingleOrDefault(s => s.Id == subcategoryId);
            if (product.ApplicationUser == null || product.SubCategory == null)
                throw new NullReferenceException("productId or userId is not valid");

            _Db.Products.Add(product);
            _Db.SaveChanges();
        }

        public void DelProduct(Int32 prodId)
        {
            _Db.Products.Remove(_Db.Products.Find(prodId));
            _Db.SaveChanges();
        }

        public Product GetProduct(Int32 prodId)
        {
            return _Db.Products.SingleOrDefault(p => p.Id == prodId);
        }
    }

}