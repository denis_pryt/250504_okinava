﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MyASPApp.Infrastructure;
using MyASPApp.Models;

namespace MyASPApp.Controllers
{
    public class ProductsController : Controller
    {
        private CatalogManager _manager = new Infrastructure.CatalogManager();

        public ActionResult ProductsList(Int32? subcategoryId)
        {
            try
            {
                return View(_manager.GetSubcategory(subcategoryId.Value));
            }
            catch (InvalidOperationException)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult ProductsTable(Int32? subcategoryId)
        {
            try
            {
                return View(_manager.GetSubcategory(subcategoryId.Value));
            }
            catch (InvalidOperationException)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Product(Int32? productId)
        {
            try
            {
                return View(_manager.GetProduct(productId.Value));
            }
            catch (InvalidOperationException)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult AddFeedback(Int32 productId, Feedback feedback)
        {
            _manager.AddFeedback(User.Identity.GetUserId(), productId, feedback);
            return RedirectToAction("Product");
        }

        public ActionResult Feedbacks(Int32? productId)
        {
            try
            {
                return View(_manager.GetProduct(productId.Value));
            }
            catch (InvalidOperationException)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Feedbacks(Int32 productId)
        {
            return View(_manager.GetFeedbacksByProduct(productId));
        }
    }
}

namespace MyASPApp.Helpers
{
    public static class ProductHelper
    {
        private static TagBuilder WrapToRow(TagBuilder tag)
        {
            var row = new TagBuilder("div");
            row.AddCssClass("row");
            row.InnerHtml += tag;
            return row;
        }

        public static MvcHtmlString CreateList(this HtmlHelper html, Product product)
        {
            var h1ProdName = new TagBuilder("h1");
            h1ProdName.SetInnerText(product.Name);

            var row1 = WrapToRow(h1ProdName);

            var container = new TagBuilder("div");
            container.AddCssClass("container");

            var img = new TagBuilder("img");
            img.Attributes["alt"] = "100%";
            img.Attributes["src"] = product.ImagePath;

            var column3 = new TagBuilder("div");
            column3.AddCssClass("col-md-3");
            column3.InnerHtml += img.ToString();

            var column9 = new TagBuilder("div");
            column9.AddCssClass("col-md-9");

            var colForRait = new TagBuilder("div");
            colForRait.AddCssClass("col-md-12 pull-left");

            var h3Rait = new TagBuilder("h3");
            h3Rait.SetInnerText("Рейтинг: " + product.Rating.ToString());
            colForRait.InnerHtml += h3Rait.ToString();

            var row2_1 = WrapToRow(colForRait);
            column9.InnerHtml += row2_1.ToString();

            var h5Descr = new TagBuilder("h5");
            h5Descr.SetInnerText(product.Description);

            column9.InnerHtml += row2_1.ToString();
            column9.InnerHtml += WrapToRow(h5Descr);

            var row2 = WrapToRow(column3);
            row2.InnerHtml += column9;
            // да ну нахер
            container.InnerHtml += row1.ToString();
            container.InnerHtml += row2.ToString();
            
            return new MvcHtmlString(container.ToString());
        }
    }
}