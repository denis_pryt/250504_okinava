﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;
using System.Web.Management;
using System.Web.Mvc;
using MyASPApp.Models;

namespace MyASPApp.Controllers
{
    public class HomeController : Controller
    {
        MyASPApp.Infrastructure.CatalogManager _manager = new Infrastructure.CatalogManager();
        public ActionResult Index()
        {
            var prodCountByCategory = new Dictionary<int, int>();
            var categoryes = _manager.GetAllCategoryes();
            foreach (var category in categoryes)
            {
                prodCountByCategory[category.Id] = _manager.GetProductsByCategory(category.Id).Count;
            }
            ViewBag.ProdCountByCategory = prodCountByCategory;
            return View(categoryes);
        }

        public ActionResult Subcategory(Int32 categoryId)
        {
            return View( _manager.GetSubcategotyesByCategory(categoryId) );
        }

        public ActionResult AddSubcategory(SubCategory subcategory, Int32 categoryId)
        {
            _manager.AddSubcategory(subcategory, categoryId);
            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult AddFeedback( string userId, Int32 prodId, Feedback feedback )
        {
            try
            {
                if (feedback == null)
                    return HttpNotFound("Invalid args");

                _manager.AddFeedback(userId, prodId, feedback);
            }
            catch
            {
                return HttpNotFound();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DelFeedback(Int32 feedbackId)
        {
            try
            {
                _manager.DelFeedback(feedbackId);
                return RedirectToAction("Index");
            }
            catch
            {
                return HttpNotFound();
            }
        }
    }
}