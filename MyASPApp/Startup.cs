﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyASPApp.Startup))]
namespace MyASPApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
