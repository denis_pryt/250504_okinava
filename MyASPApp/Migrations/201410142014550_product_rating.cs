namespace MyASPApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class product_rating : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Rating", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Rating");
        }
    }
}
