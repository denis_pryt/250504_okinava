namespace MyASPApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class store_ExchangeRate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Stores", "ExchangeRate", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Stores", "ExchangeRate");
        }
    }
}
