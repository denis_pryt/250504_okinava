﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyASPApp.Models
{
    public class Product
    {
        public Product(){}

        public Product(ApplicationUser user, SubCategory subCategory)
        {
            this.ApplicationUser = user;
            this.SubCategory = subCategory;
        }

        [HiddenInput(DisplayValue = false)]
        public virtual Int32                            Id { get; set; }
        public virtual ICollection<Feedback>            Feedbacks { get; set; }
        public virtual ApplicationUser                  ApplicationUser { get; set; }    //Создатель
        public virtual SubCategory                      SubCategory { get; set; }
        public virtual ICollection<Store>               Stores { get; set; }
        public virtual ICollection<ProductsPrices>      ProductsPrices { get; set; }
        public virtual ICollection<AttrValue>           AttrValues { get; set; }
        
        [Required(ErrorMessage = "Не забываем ввести название!")]
        [Display(Name = "Название")]
        public String   Name { get; set; }
        
        [Display(Name = "Описание")]
        public String   Description { get; set; }
        public String   ImagePath { get; set; }
        public Int32    Rating { get; set; }
    }
}