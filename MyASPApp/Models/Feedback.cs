﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyASPApp.Models
{
    public class Feedback
    {
        public Feedback() {}
        public Feedback(ApplicationUser user, Product producr)
        {
            this.ApplicationUser = user;
            this.Product = producr;
        }

        [HiddenInput(DisplayValue = false)]
        public virtual Int32            Id { get; set; }
        public virtual Product          Product { get; set; }                    // К какому продукту отзыв
        public virtual ApplicationUser  ApplicationUser { get; set; }    // Кто оставил отзыв

        [Required(ErrorMessage = "Введите оглавление!")]
        [Display(Name = "Оглавление")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Оглавление должно быть от 3 до 50 символов")]
        public String   FeedbackTitle { get; set; }

        [Required(ErrorMessage = "Введите отзыв!")]
        [Display(Name = "Текст отзыва")]
        public String   FeedbackText { get; set; }
        
        [Display(Name = "Оценка")]
        public int      FeedbackRate { get; set; }

        public DateTime DateTime { get; set; }

        //public virtual DateTime FeedbackDateTime { get; set; }
    }
}