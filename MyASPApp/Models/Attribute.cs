﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyASPApp.Models
{
    public class Attribute
    {
        public Int32                            Id { get; set; }
        public virtual ICollection<SubCategory> SubCategories { get; set; }
        public virtual ICollection<AttrValue>   AttrValues { get; set; }
         
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public String   AttrName { get; set; }
        public Int32    AttrType { get; set; }
    }
}