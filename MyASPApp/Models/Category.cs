﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyASPApp.Models
{
    public class Category
    {
        public Category() { }

        [HiddenInput(DisplayValue = false)]
        public virtual Int32                    Id { get; set; }
        public virtual ICollection<SubCategory> SubCategories { get; set; }

        [Required(ErrorMessage = "Категория без названия?")]
        [Display(Name = "Название")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public String Name { get; set; }
        public virtual String Picture { get; set; }
    }
}