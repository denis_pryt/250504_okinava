﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyASPApp.Models
{
    public class Phone
    {
        [HiddenInput(DisplayValue = false)]
        public Int32            Id { get; set; }
        public virtual Store    Store { get; set; }

        [Required(ErrorMessage = "Введите номер!")]
        [RegularExpression(@"[+8](\d){5,10}", ErrorMessage = "Введите правильный номер телефона")]
        public String   PhoneNumber { get; set; }
    }
}