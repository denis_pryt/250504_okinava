﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Drawing.Imaging;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using MyASPApp.Infrastructure;

namespace MyASPApp.Models
{
// You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public virtual ICollection<Product>         Products { get; set; }          // Созданные продукты
        public virtual ICollection<Feedback>        Feedbacks { get; set; }         // Отзывы
        public virtual ICollection<Store>           Store { get; set; }             // Магазин, если юзер -- продавец
        
        public Int32    Rate { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {}

        public DbSet<Product>           Products { get; set; }
        public DbSet<Feedback>          Feedbacks { get; set; }
        public DbSet<Category>          Categories { get; set; }
        public DbSet<SubCategory>       SubCategories { get; set; }
        public DbSet<ProductsPrices>    ProductsPriceses { get; set; }
        public DbSet<Store>             Stores { get; set; }
        public DbSet<Phone>             Phones { get; set; }
        public DbSet<Attribute>         Attributes { get; set; }
    }
}