﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyASPApp.Models
{
    public class AttrValue
    {
        public Int32                Id { get; set; }
        public virtual Product      Product { get; set; }
        public virtual Attribute    Attribute { get; set; }

        public String   Value { get; set; }
    }
}