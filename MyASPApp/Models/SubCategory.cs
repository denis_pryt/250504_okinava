﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyASPApp.Models
{
    public class SubCategory
    {
        public virtual Int32                    Id { get; set; }
        public virtual ICollection<Product>     Products { get; set; }
        public virtual Category                 Category { get; set; }
        public virtual ICollection<Attribute>   Attributes { get; set; }

        [Required(ErrorMessage = "Категория без названия?")]
        [Display(Name = "Название подкатегории")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public virtual String Name { get; set; }
        public virtual String Picture { get; set; }
    }
}